import React, { Component } from 'react';
import Slide from "../Components/Slide";
import "./SlideShow.css";

class SlideShow extends Component {
    constructor (props)
    {
        super(props);
        this.state = {
            basePath: '/images/slides/',
            slides: [
                {src: 'solving_problems.png', caption: 'solving problems'},
                {src: 'acting_serious.png', caption: 'acting serious'},
                {src: 'looking_ahead.png', caption: 'looking ahead'},
                {src: 'dreaming_dreams.png', caption: 'dreaming about dreams'}
            ],
            activeSlide: 0
        }
    }
    componentDidMount()
    {
        this.intervall = setInterval(this.switchActiveSlide, 5000);
    }
    render ()
    {
        let basePath = this.state.basePath;
        let slides   = [];

        for (let slideIndex in this.state.slides) {
            let slide   = this.state.slides[slideIndex];
            let imgSrc  = basePath + slide.src;
            let caption = slide.caption;
            let active  = (parseInt(slideIndex) === this.state.activeSlide);
            slides.push(
                <Slide key={slideIndex} src={imgSrc} className={active} caption={caption} activeSlide={active}/>
            );
        }

        return (
            <div className="slideShow">
                {slides}
            </div>
        );
    }
    switchActiveSlide = () => {
        // Randomize next slide
        while (true) {
            var nextSlide = Math.floor(Math.random() * this.state.slides.length);
            if (nextSlide !== this.state.activeSlide) {
                break;
            }
        }

        this.setState({activeSlide: nextSlide});
    }
}

export default SlideShow