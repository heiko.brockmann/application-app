import React, { Component } from "react";
import * as THREE from "three";
import GLTFLoader from "three-gltf-loader";
import "./UnicornBG.css";

class UnicornBG extends Component {
    propTypes: {
        onModelLoaded: React.PropTypes.func
    };
    setUpLight()
    {
        var dirLight = new THREE.DirectionalLight( 0xffffff, 2.38 );
        dirLight.position.set( 2.703, -3.247, 5.882 );
        this.scene.add( dirLight );
        dirLight = new THREE.DirectionalLight( 0xffffff, 4.08 );
        dirLight.position.set( -3.390, -1.053, -.02 );
        this.scene.add( dirLight );
    }
    prepareAnimation()
    {
        // Created axis vector and animation Quaternions for rotation
        var axis          = new THREE.Vector3( 0, 0, 1 );
        var qInitial      = new THREE.Quaternion().setFromAxisAngle( axis, 0 );
        var qIntermediate = new THREE.Quaternion().setFromAxisAngle( axis, Math.PI );
        var qFinal        = new THREE.Quaternion().setFromAxisAngle( axis, Math.PI*2 );

        // Create KeyframeTrack for rotation animation
        var quaternionKF  = new THREE.QuaternionKeyframeTrack(
            '.quaternion',
            [ 0, 15, 30 ],
            [
                qInitial.x,
                qInitial.y,
                qInitial.z,
                qInitial.w,
                qIntermediate.x,
                qIntermediate.y,
                qIntermediate.z,
                qIntermediate.w,
                qFinal.x,
                qFinal.y,
                qFinal.z,
                qFinal.w,
            ]
        );
        // Create clip from Keyframes
        var rotation = new THREE.AnimationClip( 'Rotation', 30, [ quaternionKF ] );

        // Create keyframes and clip for fade in animation
        var opacityKF = new THREE.NumberKeyframeTrack( '.material.opacity', [ 0, 1 ], [ 0, 1 ] );
        var fadeIn    = new THREE.AnimationClip( 'FadeIn', 1, [ opacityKF ] );

        // Create animation mixer on mesh to hold clips
        this.mixer = new THREE.AnimationMixer( this.unicornMesh );

        // add clips and configure actions
        var rotationAction = this.mixer.clipAction( rotation );
        rotationAction.play();

        var fadeInAction = this.mixer.clipAction( fadeIn );
        fadeInAction.setLoop( THREE.LoopOnce ); //play only once
        fadeInAction.clampWhenFinished = true; //and keep props from last frame
        fadeInAction.play();
    }
    loadUnicorn ()
    {
        var loader = new GLTFLoader();
        loader.load(
            '/assets/unicorn/unicorn.gltf',
            // called when the resource is loaded
            this.loadModelHandler,
            // called while loading is progressing
            function ( xhr ) {
                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
            },
            // called when loading has errors
            function ( error ) {
                console.log( error );
            }
        );
    }
    componentDidMount()
    {
        const width  = this.mount.clientWidth;
        const height = this.mount.clientHeight;

        // Create clock to calculate delta in scene render
        this.clock = new THREE.Clock();

        // Create and setup scene
        this.scene = new THREE.Scene();
        this.initializeCamera();
        this.setUpLight();
        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        this.renderer.setSize( width, height );
        window.addEventListener('resize', this.windowResizeHandler );

        // Load model to be displayed
        this.loadUnicorn();
    }
    componentWillUnmount()
    {
        this.stopAnimation();
        this.mount.removeChild( this.renderer.domElement );
    }
    initializeCamera()
    {
        this.camera = new THREE.PerspectiveCamera(
            50,
            this.mount.clientWidth / this.mount.clientHeight,
            0.1,
            1000
        );
        this.camera.position.x = -0.573;
        this.camera.position.y = -4.884;
        this.camera.position.z = 4.824;
        this.camera.rotation.x = 68.29 * Math.PI / 180;
        this.camera.rotation.y = 9.21 * Math.PI / 180;
        this.camera.rotation.z = -21.90 * Math.PI / 180;
    }
    startAnimation()
    {
        this.mixer.timeScale = 1; // unpause animations
        if ( !this.frameId ) {
            this.frameId = requestAnimationFrame( this.animationFrameHandler );
        }
    }
    stopAnimation()
    {
        this.mixer.timeScale = 0; //really Pause animations
        cancelAnimationFrame( this.frameId );
    }
    renderScene()
    {
        this.renderer.render( this.scene, this.camera );
    }
    render()
    {
        return (
            <div className="unicornBG" ref={(mount) => { this.mount = mount }}>
            </div>
        );
    }

    animationFrameHandler = () => {
        var delta = this.clock.getDelta();

        if ( this.mixer ) {
            this.mixer.update( delta );
        }

        this.renderScene();
        this.frameId = window.requestAnimationFrame( this.animationFrameHandler );
    };
    loadModelHandler = ( modelObject ) => {
        this.scene.add( modelObject.scene );
        // Select right mesh, because I messed up creation
        this.unicornMesh = modelObject.scene.children[0].children[0];
        // Adjust material properties for the same reason
        this.unicornMesh.material.metalness = 1;
        this.unicornMesh.material.roughness = .52;
        // initially hide model to enable smooth fade in
        this.unicornMesh.material.opacity = 0;

        this.prepareAnimation();
        this.mount.appendChild(this.renderer.domElement);
        this.startAnimation();
        this.props.onModelLoaded();
    };
    windowResizeHandler = () => {
        this.camera.aspect = this.mount.clientWidth / this.mount.clientHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.mount.clientWidth, this.mount.clientHeight);
    };
}

export default UnicornBG;