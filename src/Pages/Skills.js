import React, { Component } from "react";
import SkillMatrix from "../Components/SkillMatrix";
import "./Skills.css";

class Skills extends Component {
    render()
    {
        return (
            <div>
                <h2>What I am able to offer</h2>
                <div>
                    <SkillMatrix/>
                </div>
            </div>
        );
    }
}

export default Skills;