import React, { Component } from "react";

class Motivation extends Component {
    render()
    {
        return (
            <div>
                <h2>My motivation for applying to you</h2>
                <p>
                    As I mentioned in the application letter,
                    I wanted to work in the game industry ever since I knew I wanted to work with computers.<br/>
                    Video and computer games take the most of my spare time ever since I got
                    my sister's old Commodore 64 as a child. After I got an old PC from her,
                    I began to discover the inner workings of it and built my first websites.<br/>
                    I tried to make them dynamic and of course build simple games using various different
                    technologies like PHP, JavaScript, Java Applets and Flash. <br/>
                    As I grew older my passion for gaming and my skills for creating web apps got separated,
                    one became my hobby and the other one became my job.
                    I now hope to bring them back together again.
                </p>
                <p>
                    I also experienced, that the most important thing to keep me engaged in my work, is the
                    relationship with my team members. <br/> We spent most of our lives working which means we also share
                    most of our life with our colleagues. This is why I am impressed by the family vibe I get from
                    watching your streams and seeing your pictures on twitter. <br/>
                    I'm not naive and I know that this is just a very small peek into your company,
                    but I still think, that you have a very special and positive atmosphere.
                    This Atmosphere is something I really miss in most of the companies I've got to know
                    and is exactly what I'm looking for.
                </p>
                <p>
                    I have a really broad but also deep experience in many aspects of web development.
                    If I compare myself to other web developers I know and see how many of them like to specialize
                    on a very fine-grained level, I realize how much of a "real" full-stack developer I am. <br/>
                    I sure have some better and some worse skills, but I have a huge potential which I cannot fully
                    use in my current company. On your website, I read that besides the react skills you are looking
                    for experience in server-side development, DevOps and large scaling of applications. I am sure
                    I can provide this experience to you. Because of my current position, I also have a really
                    good understanding of the architecture of web applications.
                </p>
            </div>
        );
    }
}

export default Motivation;