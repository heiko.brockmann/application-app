import React, { Component } from "react";
import "./Timeline.css";

class Timeline extends Component {
    render()
    {
        return (
            <div>
                <h2>My Career so far</h2>
                <table className="timeline">
                    <thead>
                    <tr className="headline">
                        <th>Period</th>
                        <th>Position</th>
                        <th>Employer / Institution</th>
                        <th>Short description</th>
                    </tr>
                    </thead>
                    <tbody className="career">
                    <tr>
                        <td>November 2018</td>
                        <td>Decided to apply to MediaMolecule</td>
                        <td></td>
                        <td>Created this Application</td>
                    </tr>
                    <tr>
                        <td>January 2017 - now</td>
                        <td>Software Architecture Expert</td>
                        <td><a href="https://www.eventim.de/tickets.html?language=en">CTS Eventim Solutions GmbH</a></td>
                        <td>Servant leader responsible for the technical decisions of a development Team</td>
                    </tr>
                    <tr>
                        <td>April 2015 - December 2016</td>
                        <td>Senior PHP Developer</td>
                        <td><a href="https://www.eventim.de/tickets.html?language=en">CTS Eventim Solutions GmbH</a></td>
                        <td>
                            Development of a&nbsp;
                            <a href="https://www.eventimsports.com/en/products/ticketing/">SaaS platform for ticketing</a>,
                            localisation for italian market
                        </td>
                    </tr>
                    <tr>
                        <td>August 2013 - March 2015</td>
                        <td>Full-Stack Web Developer</td>
                        <td><a href="https://www.buenting-ecommerce.de/home.html">B&uuml;nting E-Commerce</a></td>
                        <td>Development of a proprietary B2C shop software used on various distribution channels</td>
                    </tr>
                    <tr>
                        <td>April 2011 - July 2013</td>
                        <td>Full-Stack Web Developer</td>
                        <td><a href="https://www.emotion-24.co.uk/about-us">Emotion Warenhandels GmbH</a></td>
                        <td>Development of a international (UK, France, Germany) B2C shop for bathroom furniture</td>
                    </tr>
                    <tr>
                        <td>January 2011 - April 2011</td>
                        <td>Full-Stack Web Developer</td>
                        <td><a href="https://wol.de/">Westermann GmbH</a></td>
                        <td>Development of various CMS based agency client websites, conceptual work on a web to print project</td>
                    </tr>
                    <tr>
                        <td>October 2008 - December 2010</td>
                        <td>Full-Stack Web Developer & IT Administrator</td>
                        <td><a href="http://www.druckdeal.de/">Druckbesser GmbH & Co KG</a></td>
                        <td>Development of a proprietary search engine for web to print services</td>
                    </tr>
                    <tr>
                        <td>April 2008 - October 2008</td>
                        <td>Household help</td>
                        <td>Mobiler Hilfsdienst der AWO (Mobile social care of the workers' welfare)</td>
                        <td>Continuation of the work I did for my civilian service while I was looking for jobs</td>
                    </tr>
                    <tr>
                        <td>August 2007 - April 2008</td>
                        <td>Civilian service</td>
                        <td>Mobiler Hilfsdienst der AWO (Mobile social care of the workers' welfare)</td>
                        <td>Household help for elderly and handicapped people</td>
                    </tr>
                    </tbody>
                    <tbody className="education">
                    <tr className="headline">
                        <th colSpan="4"><h3>Education</h3></th>
                    </tr>
                    <tr>
                        <td>August 2006 - August 2007</td>
                        <td></td>
                        <td colSpan="2">Technical school for information technologies</td>
                    </tr>
                    <tr>
                        <td>August 2004 - August 2006</td>
                        <td></td>
                        <td colSpan="2">Training as technical assistant for information technologies</td>
                    </tr>
                    <tr>
                        <td>August 2002 - August 2006</td>
                        <td></td>
                        <td colSpan="2">Realschule (Secondary school)</td>
                    </tr>
                    <tr>
                        <td>August 2000 - August 2002</td>
                        <td></td>
                        <td colSpan="2">Orientierungsstufe (Orientation level)</td>
                    </tr>
                    <tr>
                        <td>August 1996 - August 2000</td>
                        <td></td>
                        <td colSpan="2">Grundschule (Elementary school)</td>
                    </tr>
                    </tbody>
                    <tbody className="other">
                    <tr>
                        <td>August 21st 1986</td>
                        <td>I was born</td>
                        <td></td>
                        <td>I can't remember it, but it was a great day for me</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Timeline;