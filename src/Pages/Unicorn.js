import React, { Component } from "react";
import ImageLink from "../Components/ImageLink";

class Unicorn extends Component {
    render()
    {
        return (
            <div>
                <h2>Why is there a unicorn in the background?</h2>
                <h3>TL;DR</h3>
                <p>
                    The unicorn statue is an asset I created for the Source Engine.<br/>
                    I sculpted it in <a href="http://pixologic.com/sculptris/">sculptris</a> then
                    created a low poly mesh and rigged it to bring it into a dynamic pose using&nbsp;
                    <a href="https://www.blender.org/">blender</a>.<br/>
                    And I like it. What not to like about it? It's a unicorn!
                </p>
                <h3>The long story</h3>
                <p>
                    My wife, her brother and I love to play prop hunt in gmod but beside that my wife is not into games that much
                    and therefore has nothing else in her steam library. This is pretty annoying because a lot of prop hunt maps
                    use assets from other source games. So I started to create a prop hunt map with original assets for us.
                    I had an idea of a large hall with a giant unicorn statue in the centre and came up with a story.
                    I imagined a church that got rid of all their crosses in favour of unicorns so they could appeal
                    to a young audience. Unfortunately, I never found the time to finish the map.
                </p>
                <div className="gallery">
                    <ImageLink imgSrc="/images/unicornis.jpg"/>
                </div>
            </div>
        );
    }
}

export default Unicorn;