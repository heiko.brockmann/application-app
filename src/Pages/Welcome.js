import React, { Component } from "react";

class Welcome extends Component {
    render()
    {
        return (
            <div>
                <h2>Welcome molecules!</h2>
                <p>
                    Thank you very much for taking your time and having a look at my application.
                </p>
                <p>
                    Please feel free to have a look around and discover more about me, my personality,
                    my career and my skills. <br/>
                    If you are more into the technical stuff, don't hesitate to inspect everything and
                    check out the&nbsp;
                    <a href="https://gitlab.com/heiko.brockmann/application-app">git repository</a>.
                </p>
                <p>
                    Don't worry about your privacy. This site has no tracking installed and will
                    not store any data whatsoever.
                </p>
                <p>
                    I hope we will see each other in person soon.
                </p>
            </div>
        );
    }
}

export default Welcome;