import React, { Component } from 'react';
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import UnicornBG from "./Widgets/UnicornBG";
import SlideShow from "./Widgets/SlideShow";
import Welcome from "./Pages/Welcome";
import Motivation from "./Pages/Motivation";
import Timeline from "./Pages/Timeline";
import Unicorn from "./Pages/Unicorn";
import Skills from "./Pages/Skills";
import './App.css';

class App extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            unicornVisible: false
        }
    }
    render()
    {
        return (
            <div className="App">
                <HashRouter>
                    <div className="wrapper">
                        <header>
                            <ul className="navigation">
                                <li><NavLink exact to="/">Welcome</NavLink></li>
                                <li><NavLink to="/motivation">Motivation</NavLink></li>
                                <li><NavLink to="/timeline">Timeline (CV)</NavLink></li>
                                <li><NavLink to="/skills">Skills</NavLink></li>
                                <li className={this.unicornVisibleClass()}>
                                    <NavLink to="/unicorn">Unicorn?</NavLink>
                                </li>
                            </ul>
                        </header>
                        <div className="content">
                            <Route exact path="/" component={Welcome} />
                            <Route path="/motivation" component={Motivation} />
                            <Route path="/timeline" component={Timeline} />
                            <Route path="/skills" component={Skills} />
                            <Route path="/unicorn" component={Unicorn} />
                        </div>
                    </div>
                </HashRouter>
                <SlideShow/>
                <UnicornBG onModelLoaded={this.handleModelLoaded} />
            </div>
        );
    }
    unicornVisibleClass()
    {
        let visibleClass = this.state.unicornVisible ? ' visible' : '';
        return `unicornLink${visibleClass}`;
    }
    handleModelLoaded = () => {
        this.setState({
            unicornVisible: true
        });
        console.log('Wild UNICORN appeared!');
    };
}

export default App;
