import React, { Component } from "react";
import Skill from "./Skill";

class SkillGroup extends Component {
    render ()
    {
        let categoryMatrix = [];
        let filter         = this.props.filter;
        let name           = this.props.name;

        for (let skill in this.props.skills) {
            let skillLevel = this.props.skills[skill];

            // When filter is set, add skill only if category name matches or skill name itself matches
            if (filter === null || name.toLowerCase().includes(filter) || skill.toLowerCase().includes(filter)) {
                categoryMatrix.push(<Skill key={skill} name={skill} level={skillLevel}/>);
            }
        }

        // Display category only if skills are present
        if (categoryMatrix.length > 0) {
            return (
                <tbody className="category">
                <tr className="catHead">
                    <th colSpan="6">{name}</th>
                </tr>
                {categoryMatrix}
                </tbody>
            );
        }

        return (null);
    }
}

export default SkillGroup;