import React, { Component } from "react";
import SkillGroup from "./SkillGroup";

class SkillMatrix extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            skills: {
                'Programming Languages' : {
                    'JavaScript' : 5,
                    'PHP' : 5,
                    'Python' : 4,
                    'Ruby' : 3,
                    'C/C++' : 3
                },
                'Frontend Frameworks': {
                    'ReactJS' : 4,
                    'Vue.js' : 4,
                    'Angular' : 2,
                    'JQuery' : 5,
                    'Backbone' : 3
                },
                'Backend Frameworks': {
                    'Express' : 4,
                    'Next' : 2,
                    'Symfony' : 5,
                    'Laravel' : 2,
                    'Slim' : 4
                },
                'Build Tools': {
                    'Webpack': 4,
                    'Gulp': 4,
                    'Grunt': 2
                },
                'Stylesheet Preprocessors' : {
                    'Sass': 4,
                    'less': 4
                },
                'Operational Tools': {
                    'Kubernetes' : 4,
                    'Docker' : 4,
                    'Ansible' : 4,
                    'ELK Stack (Elesticsearch - Logstash - Kibana)' : 3,
                    'AWS' : 2,
                    'Apache' : 5,
                    'Nginx' : 4
                },
                'Storage technologies' : {
                    'MySQL / MariaDB' : 5,
                    'PostgreSQL' : 3,
                    'Mongo DB' : 3,
                    'Redis Cache' : 4,
                    'RabbitMQ' : 2,
                    'Kaffka': 1
                },
                'Architectural Concepts & Software Design Principles' : {
                    'Micro Services' : 5,
                    'Domain Driven Design' : 4,
                    'Hexagonal Architecture': 3,
                    'Command Query Separation (CQS)': 3,
                    'Command Query Responsibility Segregation (CQRS)': 3,
                    'Event Sourcing' : 3,
                    'Model - View - Viewmodel (MVVM)' : 3,
                    'Model - View - Controller (MVC)' : 5
                },
                'UI Design Principles, UX Concepts & Tools' : {
                    'Block - Element - Modifier (BEM)' : 4,
                    'Reasonable System for CSS (RSCSS)' : 4,
                    'Atomic Design' : 4,
                    'Fabricator': 3,
                    'Nucleus': 2,
                    'Paper Prototyping': 2
                },
                '3D Graphic Tools' : {
                    'Blender' : 3,
                    'Sculptris' : 3
                },
                '2D Graphic Tools' : {
                    'Adobe Photoshop' : 4,
                    'GIMP' : 2
                }
            },
            filter: null
        }
    }

    render ()
    {
        let skillMatrix = [];
        let filter      = this.state.filter;

        for (let skillGroup in this.state.skills) {
            let skills = this.state.skills[skillGroup];
            skillMatrix.push(<SkillGroup key={skillGroup} name={skillGroup} skills={skills} filter={filter}/>);
        }

        return (
            <table className="skillMatrix">
                <thead>
                <tr>
                    <th>Skill</th>
                    <th className="skillCol"><div><span>Beginner</span></div></th>
                    <th className="skillCol"><div><span>Junior</span></div></th>
                    <th className="skillCol"><div><span>Intermediate</span></div></th>
                    <th className="skillCol"><div><span>Senior</span></div></th>
                    <th className="skillCol"><div><span>Expert</span></div></th>
                </tr>
                </thead>
                <tbody className="toolbar">
                    <tr>
                        <td colSpan="6">
                            <input placeholder="Filter skills..." onChange={this.handleFilterInput}/>
                        </td>
                    </tr>
                </tbody>
                <tbody className="emptyState">
                    <tr>
                        <td colSpan="6">
                            No matching skill or group could be found,
                            but I am pretty good at learning things.
                        </td>
                    </tr>
                </tbody>
                {skillMatrix}
            </table>
        );
    }

    handleFilterInput = (event) => {
        let filter = event.target.value.trim().toLowerCase();
        this.setState({filter: filter});
    }
}

export default SkillMatrix;