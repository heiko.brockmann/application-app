import React, { Component } from "react";
import "./ImageLink.css";

class ImageLink extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            enlarged: false
        };
    }
    render()
    {
        let classes = 'imageLink' + (this.state.enlarged ? ' enlarged' : '');

        return (
            <div className={classes} onClick={this.toggleView}>
                <img src={this.props.imgSrc} alt=""/>
            </div>
        );
    }
    toggleView = () => {
        this.setState({enlarged: !this.state.enlarged});
    }
}

export default ImageLink;