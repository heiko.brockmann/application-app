import React, { Component } from 'react';

class Slide extends Component {
    render ()
    {
        let classes = 'slide' + (this.props.activeSlide === true ? ' active' : '');
        return (
            <div className={classes}>
                <img src={this.props.src} alt={this.props.caption} />
                <span>{this.props.caption}</span>
            </div>
        );
    }
}

export default Slide;