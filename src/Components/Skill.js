import React, { Component } from "react";

class Skill extends Component {
    render()
    {
        let name = this.props.name;
        // Start matrix row with skill name
        let matrix = [
            (<td key={name}>{name}</td>)
        ];
        // Add five cells with tick symbol in the right one
        for(var i = 1; i < 6; i++) {
            if (i === this.props.level) {
                // index matches skill level, so add cell with tick
                matrix.push(<td className="skillCol" key={i}>&#10004;</td>)
            } else {
                // just render blank otherwise
                matrix.push(<td className="skillCol" key={i}>&nbsp;</td>);
            }
        }
        return (
            <tr>{matrix}</tr>
        );
    }
}

export default Skill;